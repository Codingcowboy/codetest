# Devops Code Test: 'Packaging Automation'

Welcome to the Devops code test! This will be a test of your:
1. Ability to work with an existing process
2. Familiarity with a variety of technologies
3. Design/Architect a CI process

The point of this test is to understand how you approach a technical problem. The way you code is just as important as your solution. 

*Please communicate with us via email as if you already work here, and add documentation/comments as you go along.*

We expect this test to take you about 4 - 8 hours. Given that most people are busy, We'd like to hear back from you within 72 hours of starting.

Send us an email when you're finished and we'll schedule a code review with engineers from across our teams to discuss your submission. We'll discuss the finer
 points of your implementation, walk through your code, demo its functionality (or troubleshoot its brokenness), and give feedback on its strengths, weaknesses 
 and, if we're being honest with ourselves, bugs. We don't expect your submission to be perfect, but we do expect you to be able to frankly communicate and 
 discuss your submission in a professional environment.

## Overall Requirements
Your task is to provide an automated process for packaging, testing, and distribution of the `sl` command,
a cruel program made to punish users who misspell `ls`. This package will be a debian package, and you will need to provide distribution packages for both production and development, which is controlled at build time.

You may use any technologies that you like to achieve the final outcome. You may also use any language for scripting that you like, though we would recommend use of python/bash, (as long as it isn't [Ook!](https://esolangs.org/wiki/Ook!) ). You may also make modifications to the `sl-fork` repository to accomplish part, or all, of your task.

#### As soon as you begin:
* [ ] Fork this repository into a public repository on your gitlab, github, bitbucket, etc. account
* [ ] Fork the [sl-fork](https://gitlab.com/opendrives/public/sl-fork) into a public repository on your gitlab, github, bitbucket, etc. account

#### While working on this, please:
* [ ] Commit early and often. We'll likely be following along with your progress.

#### Upon completing this, please post to us:
* [ ] A link to your git repositories such that we may view your code.
* [ ] A link to somewhere where we may view your CI process
* [ ] A link to somewhere where we may download the distributed Production Package, Version 1.0.2, and, if changes are made to `sl-fork`, 1.0.3.

## Technical Requirements

The processes for automation and distribution may be written in whatever language, and any technologies you choose, so long as it is operable after setting up the CI / distribution process (beware libraries that do not come installed by default). Assume that the package, and its dependencies have never been installed before, on a standard debian bullseye system.

### Automation
* [ ] A Process to create the package
    - [ ] Ensure that testing occurs as part of this process
    - [ ] Ensure that testing succeeds
* [ ] Package must be built for Production and Development environments
* [ ] Package name must reference its version, or version able to be determined without installation
* [ ] A Process to distribute the Production and Development packages for consumption

### Distribution
* [ ] Distribution of the `sl-fork` packages in Development and Production builds.
* [ ] A repository, or location that can be accessed from many locations.
    - [ ] Access method is your choice; HTTP / Apt Repo, NFS, SMB, SSH, FTP, anything but IPoAC.
    - [ ] Control, or Separation of Production and Development packages (Production user should not be able to access Development package, but Development user can access both)
* [ ] Should be able to support multiple versions (EX 1.0.2, 1.0.3, etc)
* [ ] Production must be downloadable by us, Development download to be demonstrated during review.

### Reproducibility
* [ ] Should be able to recreate your process, EX: Infrastructure as Code, or a set of scripts to stand up your solution.

### Documentation
* [ ] Should be able to gain a good understanding and overview of your solution, technologies used, and workflow from some form of documentation.
* [ ] Documentation of how to setup and utilize your solution
* [ ] If changes are made to the `sl-fork` repository, additionally provide documentation of changes that were made to accommodate your solution.

## BONUS

If you're so cool you wear shades while programming, and knocked out the above already, consider adding some improvements to what you have made.

### Suggestions for improvements
* [ ] Containerization, *slaps docker* This baby can hold so many single processes!
    * [ ] Distribution of Container
* [ ] Better Testing
    * [ ] Better test process, or a better test of functionality
    * [ ] Capability to download/view test reports as a developer
* [ ] Make package available in any other OS packaging (anything you want, but it must be able to be demonstrated during review)
* [ ] Edit this readme with new suggestions on how to improve this code test